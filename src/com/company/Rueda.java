package com.company;

/**
 * Created by july on 16/05/2017.
 */
public interface Rueda {

    public void avanzar();
    public void detenerse();
}
