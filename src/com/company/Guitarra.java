package com.company;

/**
 * Created by july on 16/05/2017.
 */
public class Guitarra extends InstrumentoMusical {
    @Override
    public void sonido() {
        System.out.println("La guitarra vibra sus cuerdas");
    }

    @Override
    public void tipo() {
        System.out.println("La guitarra es instrumento de cuerda");
    }
}
