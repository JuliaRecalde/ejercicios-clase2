package com.company;

/**
 * Created by july on 16/05/2017.
 */
public class Carro implements Rueda {

    //Atributos
    String placa;
    String marca;
    float precio;

    public Carro(){

    }

    public Carro(String placa, String marca, float precio) {
        this.placa = placa;
        this.marca = marca;
        this.precio = precio;
    }

    public void caracteristicas (){
        System.out.println("Placa: "+placa+"\nMarca: "+ marca+ "\nPrecio: "+precio);
    }

    @Override
    public void avanzar() {
        System.out.println("El carro avanza");
    }

    @Override
    public void detenerse() {
        System.out.println("El carro se detiene");
    }

    public void imprimir (String color){
        System.out.println("El auto es color "+color);

    }

    public void imprimir (int puertas){
        System.out.println("El auto tiene "+ puertas + " puertas");
    }
}
